import React,{Component} from "react";
import {HomePage,Postspage,SinglePostPage} from "./entities/HomePages";
import {BrowserRouter,Switch,Redirect,Route} from "react-router-dom";
import {Contactspage} from "./entities/Pages";

const changeableComponent = (values,Component) => {
    console.log(values);
    return () => {
        return(
            <Component {...Component} links={values}/>
        );
    }
}

export default class MainAppHome extends Component {
    constructor(props) {
        super(props);
        this.routesArray = [
            {path:"/posts/limit/:id",component:Postspage},
            {path:"/posts/:id",component:SinglePostPage},
            // {path:"/list/:id",component:ListPage},
            // {path:"/about",component:AboutPage},
            // {path:"/contacts",component:Contactspage},
            // {path:"",component:NotFound}];
            ]
        this.links = [
            {path:"/posts/limit/30",component:Postspage,name:"Posts"},
            // {path:"/about",component:AboutPage,name:"About"},
            // {path:"/contacts",component:Contactspage,name:"Contacts"}];
        ]
    }
    componentDidMount = () => {
        localStorage.setItem('routes',JSON.stringify(this.routesArray));
    }

    render = () => {
        const Home = changeableComponent([...this.links],HomePage);
        return(
            <div>
                <BrowserRouter>
                    <Switch>
                        <Route exact path={"/"} component={Home}/>
                        {
                            this.routesArray.map(item => (
                                <Route exact key={item.path} path={item.path} component={item.component}/>
                            ))
                        }
                        <Redirect from="/posts" to="/posts/limit/30"/>
                    </Switch>
                </BrowserRouter>
            </div>

        );
    }

}