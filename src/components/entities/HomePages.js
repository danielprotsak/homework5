import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";


export const HomePage = ({links}) => {
    console.log(links);
    return(
        <>
        <div>
            <h1>Home page</h1>
            {
                links.map((item,index) => (
                    <Link key={index} to={item.path}>{item.name}</Link>
                ))
            }
        </div>
        </>
    );
}

export const SinglePostPage = ({match}) => {
    const{params} = match;
    const[post,setPost] = useState(null);
    useEffect(()=>{
        fetch(`https://jsonplaceholder.typicode.com/posts/${params.id}`).then(resp => resp.json()).then(data => {
            setPost(data);
        })
    },[]);
    return (
        <div>
            {post !== null &&
            <>
                <h1>{post.title}</h1>
                    <div>
                        {post.body}
                    </div>
            </>
            }
            <CommentsForPost postId={params.id}/>
        </div>


    );
}
const CommentsForPost = ({postId}) => {
    const[comments,setComments] = useState([]);
    const[showComments,setShowComments] = useState(false);
    const showCommentsHandle = _ => {
        setShowComments(!showComments);
    }
    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}/comments`).then(resp => resp.json()).then(data => {
        setComments(data);
    });
    return(
        <div>
            {!showComments &&
            <button onClick={showCommentsHandle}>Show comments</button>
            }

            {showComments &&
                <>
            <button onClick={showCommentsHandle}>Hide comments</button>
                <div style={{display:'inline-grid'}}>
                    <h2>Comments</h2>
                    {comments.map(item => (
                        <div  >
                            <h3>{item.email}</h3>
                            <h4>{item.name}</h4>
                            <p>{item.body}</p>
                        </div>
                    ))}
                </div>
            </>
            }


        </div>
    );
}
export const Postspage = ({history,match}) => {
    const{params} = match;
    const[postList,setPostList] = useState([]);
    const[postsAmount,setPostsAmount] = useState(params.id);
    useEffect(()=>{
        fetch('https://jsonplaceholder.typicode.com/posts').then(resp => resp.json()).then(data => {
             const refactoredList = data.map(item => {
                 return {id:item.id,title:item.title,body:item.body};
             })
            setPostList(refactoredList);
        });
    },[]);

    const loadMorePosts = _ => {
        console.log(postList.length);
        if(Number(params.id)+30 < postList.length){
            setPostsAmount(Number(params.id)+30);
            history.push(`${Number(params.id)+30}`);
        }else
            if(Number(params.id) < postList.length){
            const value =  postList.length-Number(params.id);
            history.push(`${postList.length}`);
            setPostsAmount(postList.length);
        }

    }
    return(
        <div>
            <ul>
                {
                    postList.map((item,index) => {
                       if(index < postsAmount){
                           return( <li key={item.id}><Link to={`../${item.id.toString()}`}>{item.title}</Link></li>)
                       }
                    })
                }
            </ul>
            {postsAmount !== 100 &&
                <button onClick={loadMorePosts}>Load more...</button>
            }
        </div>
    );
}
