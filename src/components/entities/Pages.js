import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";

export const HomePage = ({links}) => {
    console.log(links);
    return(
        <div>
            <nav>
                {
                    links.map(item => (
                        <div>
                            <Link key={item.path}to={item.path}>{item.name}</Link>
                        </div>
                    ))
                }
            </nav>
            <h1>Home page</h1>
        </div>
       
    );
}

export class ListPage extends React.Component{
    state={list:undefined}
    constructor(props) {
        super(props);
    }

    componentDidMount = () => {
        fetch('https://jsonplaceholder.typicode.com/albums').then(resp => resp.json()).then(data =>{
            const arr = data.map(item => item.title);
            this.setState({list:arr});
        })
    }

    render = () => {
        const{params} = this.props.match;
        const{list} = this.state;
        if(this.state.list){
            if("id" in params){
                return (
                    <div style={{display:"inline-grid"}}>
                        <Link to="/list">Back</Link>
                        {
                            list.filter((item,index) => {
                                if(item === params["id"]){
                                    return (
                                        <li key={index}>{item}</li>
                                    );
                                }
                            })
                        }
                    </div>
                );
            }else{
                return(
                    <div style={{display:"inline-grid"}}>
                        <Link to="/">Back</Link>
                        {!("id" in params) &&
                        list.map((item,index) => (
                            <li key={index}><Link to={`/list/${item}`}>{item}</Link></li>
                        ))
                        }
                    </div>
                );
            }
        }else{
            return null;
        }
    }


}


export const AboutPage = () => {
    return(
        <div style={{display:"inline-grid"}}>
            <Link to="/">Back</Link>
            <span>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci error
            ipsum nobis numquam perspiciatis suscipit!
            Ad aut commodi consequatur enim esse facilis laborum libero, nisi nulla officia saepe ut voluptatem.
        </span>
        </div>

    );
}

export const Contactspage = () => {
    const contactList = ["+3804561321","+3805612311","+3885713254","+380634562132"];
    return(
        <div style={{display:"inline-grid"}}>
            <Link to="/">Back</Link>
            <ul>
                {
                    contactList.map((item,index) => (
                        <li key={index}>{item}</li>
                    ))
                }
            </ul>

        </div>
    );
}
export const NotFound = () => {
    return(
        <h1>Not found</h1>
    );
}