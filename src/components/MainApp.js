import React,{Component} from "react";
import {HomePage,ListPage,NotFound,AboutPage,Contactspage} from "./entities/Pages";
import {BrowserRouter,Switch,Redirect,Route} from "react-router-dom";

const changeableComponent = (values,Component) => {
    console.log(values);
    return () => {
        return(
            <Component links={values}/>
        );
    }
}
export default class MainApp extends Component {
    constructor(props) {
        super(props);
        this.routesArray = [
            {path:"/list",component:ListPage},
            {path:"/list/:id",component:ListPage},
            {path:"/about",component:AboutPage},
            {path:"/contacts",component:Contactspage},
            {path:"",component:NotFound}];
        this.links = [
            {path:"/list",component:ListPage,name:"List"},
            {path:"/about",component:AboutPage,name:"About"},
            {path:"/contacts",component:Contactspage,name:"Contacts"}];
    }

    render = () => {
        const Home = changeableComponent([...this.links],HomePage);
        return(
            <div>
                <BrowserRouter>
                    <Switch>
                        <Route path="/" exact component={Home}/>
                        {
                            this.routesArray.map(item => (
                               <Route key={item.path} exact sensitive path={item.path} component={item.component}/>
                            ))
                        }
                    </Switch>

                </BrowserRouter>
            </div>

        );
    }

}